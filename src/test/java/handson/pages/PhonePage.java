package handson.pages;

import org.openqa.selenium.support.PageFactory;

public class PhonePage extends Page {

    public PhonePage() {
        super("/phones");
        PageFactory.initElements(webDriver, this);
    }

}
