package handson.pages;

import handson.webdriver.SharedDriver;
import org.junit.Assert;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.support.ui.LoadableComponent;

import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;

/**
 * A generic page. Other pages will inherit from this object.
 */
public abstract class Page extends LoadableComponent {

    private final String relativePath;
    public ResourceBundle resourceBundle;
    final SharedDriver webDriver;

    public Page(String relativePath) {
        this.webDriver = SharedDriver.newInstance();
        this.relativePath = relativePath;
        this.resourceBundle = ResourceBundle.getBundle("properties/general");

    }

    /**
     * Verifies that the current page is loaded.
     */
    protected void isLoaded() {
        Assert.assertTrue(
                "The page was not loaded correctly." +
                        "\nActual URL: " + webDriver.getCurrentUrl() +
                        "\nExpected URL: " + resourceBundle.getString("baseUrl") + this.relativePath

                , webDriver.getCurrentUrl().equals(resourceBundle.getString("baseUrl") + this.relativePath)
        );
    }

    /**
     * Loads the current page.
     */
    protected void load() {

        webDriver.get(resourceBundle.getString("baseUrl") + this.relativePath);
    }

    public String getRelativePath() {
        return this.relativePath;
    }

    protected static void waitForAngularRequestsToFinish(JavascriptExecutor driver) {
        driver.executeAsyncScript("var callback = arguments[arguments.length - 1];" +
                "angular.element(document.body).injector().get('$browser').notifyWhenNoOutstandingRequests(callback);");
    }

}









