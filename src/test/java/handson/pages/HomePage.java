package handson.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

public class HomePage extends Page {

    public HomePage() {
        super("/");
        PageFactory.initElements(webDriver, this);
    }

    @FindBy(linkText = "Phone Catalog")
    private WebElement lnkEnterSUT;

    public PhonePage openPhoneCatalog() {
        lnkEnterSUT.click();
        return new PhonePage();
    }

}
