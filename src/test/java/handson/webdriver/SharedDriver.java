package handson.webdriver;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.events.EventFiringWebDriver;

import java.util.concurrent.TimeUnit;

public class SharedDriver extends EventFiringWebDriver {

    private static SharedDriver instance = null;
    private static WebDriver REAL_DRIVER;

    static {
        REAL_DRIVER = new FirefoxDriver();
        REAL_DRIVER.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
        REAL_DRIVER.manage().timeouts().setScriptTimeout(30, TimeUnit.SECONDS);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                REAL_DRIVER.quit();
            }
        });
    }

    public static SharedDriver newInstance() {
        if (instance == null) {
            instance = new SharedDriver();
        }
        return instance;
    }

    public SharedDriver() {
        super(REAL_DRIVER);
    }

}