package handson.stepdefs;

import cucumber.api.PendingException;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import handson.pages.HomePage;

public class PhoneCatalogSteps {

    HomePage home = new HomePage();

    @Given("^that I have opened the phonebook$")
    public void that_I_have_opened_the_phonebook() throws Throwable {
        home.get();
        ;

    }

    @When("^I search for the phone \"(.*?)\"$")
    public void i_search_for_the_phone(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

    @Then("^the phones matching \"(.*?)\" are displayed$")
    public void the_phones_matching_are_displayed(String arg1) throws Throwable {
        // Write code here that turns the phrase above into concrete actions
        throw new PendingException();
    }

}
