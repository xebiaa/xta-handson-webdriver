Feature: Phone catalog

  As a hands-on participant
  I want to be able to do basic interactions on a SUT
  So that I can explore the basics of Selenium WebDriver

  Scenario: Searching for a phone
    Given that I have opened the phonebook
    When I search for the phone "Nexus"
    Then the phones matching "Nexus" are displayed


