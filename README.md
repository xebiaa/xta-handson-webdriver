#Webdriver handson


###Importing the project in IntelliJ
After cloning this git repository, choose import project in IntelliJ and select the build.grade file. When prompted, choose the default gradle wrapper.

###Running the tests
The primary way to execute the tests is by running the cucumber gradle task:

####On OS X
`./gradlew cucumber`

####On Windows
`gradlew cucumber`

####From IntelliJ
The gradle tasks can be run directly from IntelliJ. Alternatively, the cucumber plugin can be used. For the plugin to work, a run configuration might have to be created. The fastest way to do this is to select a feature file and press run. This creates a default run config which can be edited. Edit the config, under glue type 'handson.stepdefs' and apply the changes.